<?php

namespace ebogun\easyconfig;

/**
 * Class EasyConfig
 * Library for easy work with config files
 *
 * @property array $config The config array
 *
 * @package ebogun\easyconfig
 * @since 1.2
 */
class EasyConfig
{
    public $file;

    /**
     * @var array the config data
     */
    public $data;
    /**
     * @var string the used flag
     */
    public $used;

    /**
     * @param array $config data fro set default config
     * @param string $used default used for config
     */
    function __construct($config = [], $used = ''){
        $this->data = $config;
        $this->used = $used;
    }

    /**
     * Returns the value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $object->property;`.
     * @param string $name the property name
     * @return mixed the property value
     * @param $name
     * @return mixed
     * @throws \Exception
     * @see __set()
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (method_exists($this, 'set' . $name)) {
            throw new \Exception('Getting write-only property: ' . get_class($this) . '::' . $name);
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    /**
     * Sets value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$object->property = $value;`.
     * @param string $name the property name or the event name
     * @param mixed $value the property value
     * @throws \Exception
     * @see __get()
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (method_exists($this, 'get' . $name)) {
            throw new \Exception('Setting read-only property: ' . get_class($this) . '::' . $name);
        } else {
            throw new \Exception('Setting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    /**
     * Get config with new default used
     *
     * @param $name
     * @return EasyConfig
     */
    public function used($name){
        return new EasyConfig($this->getConfig($name));
    }

    /**
     * Find all arrays with $key
     * @param string $key The key for find in config
     * @param mixed $value The key value for find
     * @param string $condition Condition for comparison element with key value. Valid conditions is >, <, = or ==,
     * @return array
     */
    public function find($key, $value, $condition = '='){
        return $this->filter($this->data, $key, $value, $condition);
    }

    /**
     * Find first array with $key
     * @param $key
     * @param $value
     * @param string $key The key for find in config
     * @param mixed $value The key value for find
     * @param string $condition Condition for comparison element with key value.
     * Valid conditions is = or ==,>, <, ! or != or <>, >=, <=, === used for comparison by type
     * @return array
     */
    public function findOne($key, $value, $condition = '='){
        return $this->filter($this->data, $key, $value, $condition)[0];
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     * Checking value availability in data list
     */
    public function issetValue($key, $value){
        return isset($this->filter($this->data, $key, $value, '=')[0]);
    }

    /**
     * Bind one config values to other
     * @param string $from Source key of config data
     * @param string $to Receiver key for source data
     * @param string|int $fromKey Source key name for binding
     * @param string|int $toKey Receiver Key name for binding
     * @param bool|string $name Name of binding int new config data
     * @return EasyConfig
     */
    public function bind($from, $to, $fromKey, $toKey, $name = false){
        $fromUsed = $this->used($from);
        $toConfig = $this->used($to)->config;
        foreach($toConfig as &$config){
            if(isset($config[$toKey])){
                $findResult = [];
                if(is_array($config[$toKey])){
                    foreach($config[$toKey] as $val){
                        $findResult = array_merge($findResult,$fromUsed->find($fromKey, $val));
                    }
                } else {
                    $findResult = array_merge($findResult, $fromUsed->find($fromKey, $config[$toKey]));
                }
                if($name){
                    $config[$name] = $findResult;
                }else
                    $config[$toKey] = $findResult;
            }
        }
        return new EasyConfig($toConfig);
    }

    /**
     * Return config data by name
     * @param string $name Path to config elements in array
     * For example
     * [
     *  'foo' => [
     *      ['id' => 1],
     *      ['id' => 2]
     *  ],
     *  'foo2' => 'value',
     *  '1',
     * ]
     * getConfig('@foo') => [['id' => 1],['id' => 2]]
     * getConfig('@foo@1') =>['id' => 2]
     * getConfig('@foo@0@id') => 2
     * getConfig('@foo2') => value
     * getConfig('@2') => '1'
     * @return array
     */
    private function getConfig($name = ''){
        if(empty($name))
            $name = $this->used;

        $keys = $this->parseName($name);
        $value = $this->data;
        foreach($keys as $key){
            if(isset($value[$key])){
                $value = $value[$key];
            } else {
                break;
            }
        }
        return $value;
    }

    /**
     * Set config data
     * @param $data
     */
    private function setConfig($data){
        $this->data = $data;
    }

    /**
     * Filter data by condition
     * @param mixed $array
     * @param mixed $matchKey
     * @param mixed $matchValue
     * @param string $condition
     * @return array Find result
     */
    private function filter($array, $matchKey, $matchValue,$condition)
    {
        $result = [];
        if(is_array($array)){
            foreach($array as $key => $element){
                if($key === $matchKey){
                    if(is_array($element)){
                        foreach($element as $e){
                            if($this->checkData($e, $matchValue,$condition)){
                                $result[] = $array;
                                break;
                            }
                        }
                    } elseif($this->checkData($element, $matchValue,$condition)){
                        $result[] = $array;
                        break;
                    }
                } else{
                    if(is_array($element)){
                        $result = array_merge($result, $this->filter($element, $matchKey, $matchValue,$condition));
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param mixed $data The key for find in config
     * @param mixed $value The key value for find
     * @param string $condition Condition for comparison element with key value.
     * Valid conditions is = or ==,>, <, ! or != or <>, >=, <=, === used for comparison by type
     * @return bool
     */
    private function checkData($data, $value, $condition){
        switch($condition){
            case '=':
            case '==':
                return $data == $value;
            case '>':
                return $data > $value;
            case '<':
                return $data < $value;
            case '<>':
            case '!':
            case '!=':
                return $data != $value;
            case '>=':
                return $data >= $value;
            case '<=':
                return $data <= $value;
            case '===':
            default:
                return $data === $value;
        }
    }

    /**
     * Explode path by delimiter
     * @param string $name Path in config
     * @param string $delimiter Delimiter for path in data config
     * @return array
     */
    private function parseName($name, $delimiter = '@'){
        return array_diff(explode($delimiter, $name), ['']);
    }
}