Easy Config
===========
Library for easy work with config files

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist ebogun/easyconfig "*"
```

or add

```
"ebogun/easyconfig": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
$configData = [
    'foo' => [
        ['id' => 1, 'name' => 'foo1.1'],
        ['id' => 2, 'name' => 'foo1.2'],
        ['id' => 3, 'name' => 'foo1.3'],
        ['id' => 4, 'name' => 'foo1.4'],
    ],
    'foo2' => 'value',
    '1',
];
$config = new EasyConfig($configData);
$config->config; // return $configData array;
$config->config = $configData; // set $configData as config;
//Get subConfig by config data
$config->used('@foo');// return new EasyConfig with config data
//[
//    ['id' => 1, 'name' => 'foo1.1'],
//    ['id' => 2, 'name' => 'foo1.2'],
//    ['id' => 3, 'name' => 'foo1.3'],
//    ['id' => 4, 'name' => 'foo1.4'],
//]
$config->used('@foo@1');// return new EasyConfig with config data ['id' => 1, 'name' => 'foo1.1']
$config->used('@foo@3@name');// return new EasyConfig with config data foo1.4
$config->used('@foo2');// return new EasyConfig with config data value
$config->used('@2');// return new EasyConfig with config data 1
//Find value in config

$configData = [
    'foo' => [
        ['id' => 1, 'name' => 'foo1.1'],
        ['id' => 2, 'name' => 'foo1.2'],
        ['id' => 3, 'name' => 'foo1.3'],
        ['id' => 4, 'name' => 'foo1.4'],
    ],
    'foo' => [
        ['id' => 1, 'name' => 'foo2.1'],
        ['id' => 2, 'name' => 'foo2.2'],
        ['id' => 3, 'name' => 'foo2.3'],
        ['id' => 4, 'name' => 'foo2.4'],
    ],
];
$config->find('id', 2);//Return array with find values ['id' => 1, 'name' => 'foo1.1'] and ['id' => 1, 'name' => 'foo2.1']
$config->find('id', 3, '>');//Return ['id' => 4, 'name' => 'foo1.4'] and ['id' => 4, 'name' => 'foo2.4']
$config->find('name', 'foo1.1');//Return ['id' => 1, 'name' => 'foo1.1']
TODO:Add examples code yet for bind function
```